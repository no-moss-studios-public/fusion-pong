using Fusion;
using NoMossStudios.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace FusionPong.UI
{
    public class UI_Menu : Singleton<UI_Menu>
    {
        [SerializeField] private Button quickPlayButton;
        [SerializeField] private Button autoModeButton;
        [SerializeField] private TMP_InputField sessionNameInputField;
        [Space]
        [SerializeField] private GameObject menuPanel;
        [SerializeField] private GameObject backgroundPanel;
    
        private void Start()
        {
            quickPlayButton.onClick.AddListener(() =>
            {
                HandleQuickPlayButtonPressed();
            });
            
            autoModeButton.onClick.AddListener(() =>
            {
                HandleAutoModeButtonPressed();
            });
            
            sessionNameInputField.onValueChanged.AddListener((text) =>
            {
                Debug.Log($"Session Code: {text}");
            });
        }
        
        private static void HandleQuickPlayButtonPressed()
        {
            Instance.quickPlayButton.interactable = false;
            Instance.autoModeButton.interactable = false;
            
            App.Instance.StartQuickGame(GameMode.AutoHostOrClient);
        }
    
        private static void HandleAutoModeButtonPressed()
        {
            Instance.quickPlayButton.interactable = false;
            Instance.autoModeButton.interactable = false;
            
            App.Instance.StartRoomGame(GameMode.AutoHostOrClient, Instance.sessionNameInputField.text);
        }
    
        public static void SetMenuUiVisible(bool visible)
        {
            Instance.menuPanel.SetActive(visible);
            Instance.backgroundPanel.SetActive(visible);
        }
    }
}
