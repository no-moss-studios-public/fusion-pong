Fusion Pong Sample Project
https://www.youtube.com/watch?v=lLSv1memaBA

Created by @AlCaTrAzz_Games at @No_Moss_Studios

Built with Unity 2022.1.1
Uses free version of DoTween (http://dotween.demigiant.com/)

Join our Discord community or follow our socials for updates and resources!

⭐Discord: https://discord.gg/Ka8suskKcs

⭐Website: https://studios.nomoss.co/

⭐Twitter: https://twitter.com/NoMossStudios

⭐IG: https://www.instagram.com/nomossstudios/
